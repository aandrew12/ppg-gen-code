/* 
 * File:   RS232.c
 * Author: dyulov
 *
 * Created on April 16, 2013, 11:23 PM
 */
#include <xc.h>
// Standard library headers
#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include <stdio.h>
// Peripheral library headers
#include <usart.h>

#include "RS232.h"

typedef struct{
    uint16_t TestInput;
    uint16_t SetPoint;
    uint16_t Isense;
    uint16_t Vsense;
}AnaInputs_t;
AnaInputs_t AnaInputs; // 10-bit ADC values

// Configure RS-232 Comms
void RS232_Init(void){
    Close1USART(); // Close in case USART was previously open
    // With spbrg = 207 Baudrate is 19.2K at FOSC = 64MHz and USART_BRGH_HIGH. spbrg = Fosc/Baudrate/16-1
    // With spbrg = 103 Baudrate is 9615 at FOSC = 64MHz and USART_BRGH_LOW. spbrg = Fosc/Baudrate/
    Open1USART (USART_TX_INT_OFF | USART_RX_INT_OFF | USART_ASYNCH_MODE | USART_EIGHT_BIT | USART_CONT_RX | USART_BRGH_LOW, 103);
    baud1USART (BAUD_8_BIT_RATE | BAUD_AUTO_OFF);
}

// TODO: Run in 10ms loop
// Output to RS-232.
void RS232_Write(void){
    char buf[10];

    while(Busy1USART());
    Write1USART('\r');

    sprintf(buf, "AN0:%4u ", AnaInputs.TestInput);
    while(Busy1USART());
    puts1USART((uint8_t *)buf);

    sprintf(buf, "AN1:%4u ", AnaInputs.SetPoint);
    while(Busy1USART());
    puts1USART((uint8_t *)buf);

    sprintf(buf, "AN2:%4u ", AnaInputs.Isense);
    while(Busy1USART());
    puts1USART((uint8_t *)buf);

    sprintf(buf, "AN3:%4u ", AnaInputs.Vsense);
    while(Busy1USART());
    puts1USART((uint8_t *)buf);
}