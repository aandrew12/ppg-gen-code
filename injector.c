/* PPG AA
 * File:   injector.c
 * Author: dyulov
 *
 * Created on April 16, 2013, 11:59 PM
 */
#include <xc.h>
//Standard library headers
#include <stdint.h>
#include <stdio.h>

#include <stdbool.h>
#include <stdlib.h>
#include "fixed_point.h"
#include "peripherals.h"
#include "linear_interp.h"
#include "sqrt_fixed_point.h"
#include "control.h"
#include "config.h"
#include "atemp.h"
#include "etemp.h"
#include "types.h"
#include "scheduler.h"
#include "injector.h"
#include "CalcFuelTypeParameter.h"


extern FuelType_t FuelType;
extern bool InjectorEnabled;
extern uint16_t TimeStep;

extern float BlockTemp;
float BlockTemp_0;
extern bool CalibrationMode;



extern AnalogSignal_t EngTempADC_Filtered;
extern uint16_t CAG_Offset; // 0 - 1023 = 50 - 151, 511 = 0%

int16_t TPS_VoltLo = TPS_VOLT_LO; // interp x1
int16_t TPS_VoltHi = TPS_VOLT_HI; // interp x2

int16_t StepTime=0; //do not know
int16_t StepStart=0; //do not know
int16_t StepNum =1; //tells which timestep we are in... what happens after you go past 12? 
int16_t TimeStepRun=3; //do not know --> init to 3? wtf

extern int cag_offset_warm[]; //Schedule for CAG offsets during timesteps when engine is "warm" <-- meaning what exactly?
extern int cag_offset_cold[]; //Schedule for CAG offsets during timesteps when engine is "cold" <-- meaning what exactly?
extern double time_mult_cold[]; //Schedule for fueling multipliers during timesteps " "
extern double time_mult_warm[]; //Schedule for fueling multipliers during timesteps " "

extern uint16_t TempSet;     //do not know
extern uint16_t TempThrStep12; //do not know
extern uint16_t TempThrStep11; //do not know
extern uint16_t TempThrStep9; //do not know
extern uint16_t TempThrStep7; //do not know




int16_t INJ_DutyCycleLo = INJ_DC_LO; // interp y1
int16_t INJ_DutyCycleHi = INJ_DC_HI; // interp y2
int16_t INJ_ThrottleADC = 0;
int16_t INJ_Pot = 0;
int16_t INJ_DutyCycle = 50;
int16_t INJ_ThrottleADC_old = 0;
double FuelModifier = 1.0;

float PotMult   = 1.0;
float PulseMult = 1.0;
float TempMult = 1.0;

extern uint16_t SecondsRunTime = 0;
uint16_t CountEnrichment = 10000;
uint16_t start_hot_flag=0;
int hot_flag=0;
//test
int diagnostic_mode=1;
//int start_time=5; //is this the time allowed for the engine to start?? could use this for the 45 second warmup without changing much code
int start_time = 35; //Changed to 45 seconds for my warm up period on speed controller

extern uint16_t IGN_PulsePeriod = 0;


uint16_t DC_out[]={44,44,44,46,46,46,47,47,47,49,49,49,49,51,51,51,51,52,52,52,52,53,53,53,53,56,55,55,58,58,58,58,59,60,61,62,61,61,63,63,64,64,64,65,66,66,68,68,69,69,70,70,70,71,71,72,72,73,73,73,74,74,74,74,76,76,75,75,77,77,77,78,80,81,83,82,84,83,85,88,88,88,88,88,89,89,90,91,91,92,93,93,94,94,93,93,96,95,95,96,96,97,98,99,99,99,99,100,100,103,103,104,104,104,106,106,105,107,107,108,109,108,108,109,110,110,110,112,112,112,113,113,113,116,117,117,116,119,118,118,119,119,120,120,121,121,122,123,123,123,128,128,130,129,129,131,131,131,133,133,132,132,133,134,134,135,135,135,136,136,136,138,139,138,138,138,139,142,142,142,143,143,145,146,145,146,146,150,150,149,147,148,148,148,149,149,149,151,153,155,154,154,155,155,154,156,156,156,157,158,157,158,158,158,159,159,159,160,159,160,161,162,161,162,
162};


int TPS_high[]={369, 700};  //TPS extremes for interp function, assume 800W to 500ADC, with 900W at 350
int inj_max=90;                                                                                                           

//the fuck are these two things?? 
int DC_hot_flag1[]={183, 220};  //reset relative to
int DC_hot_flag2[]={183, 220};  //{114, 337}; until r3

double time_mult;

double t_ambient = 20.0;
double rho_multiplier = 1.0; //density multiplier based on temperature

struct LINEAR_INTERP_STRUCT TPS_InjPWMInterp;
struct LINEAR_INTERP_STRUCT FM_AirTempInterp;
struct LINEAR_INTERP_STRUCT PotInterp;
struct LINEAR_INTERP_STRUCT PulseInterp;

void INJ_StartSequence(void);
void BlockTempToTimeInit(void);

// Call to use values for manual CAG calibration
void RecalibrateTPS(void){
    TPS_VoltLo = CAL_TPS_VOLT_LO; // interp x1
    TPS_VoltHi = CAL_TPS_VOLT_HI; // interp x2
    INJ_DutyCycleLo = CAL_INJ_DC_LO; // interp y1
    INJ_DutyCycleHi = CAL_INJ_DC_HI; // interp y2
    INJ_Init();
}

void INJ_Init(void) {
    DC_hot_flag1[0]=DC_out[0]*1.2;
    DC_hot_flag1[1]=DC_out[0]*2.5;
    DC_hot_flag2[0]=DC_out[0]*1.2;
    DC_hot_flag2[1]=DC_out[0]*2.9;
    INJ_DutyCycleLo = DC_out[224]; // interp y1
    INJ_DutyCycleHi = DC_out[224]*1.5;       //original value 1.47 //note in my array 208 is virtually the same as 224
    inj_max=DC_out[224]*2; //changed multiplier from 5 to 2 --- no need for 5 times max flow in any situation.... 
    PWM_LS_Init(); // Start INJ PWM output
}

/* Called from high priority ISR */
void INJ_SetDutyCycle(void){ // TODO: finish this function and test

    if (EngTempADC_Filtered.Value> TempSet+30){  //310:  100 degrees
        hot_flag = 0;
        //hot_flag=2;
    }
    else if (EngTempADC_Filtered.Value> TempSet+15){  //make 290 degrees after trial
        hot_flag = 0;
        //hot_flag=1;
    }
    else {
        hot_flag=0;
    }

    // TODO: Read inputs in a separate low priority task
    INJ_ThrottleADC_old=INJ_ThrottleADC;
    INJ_ThrottleADC = ADC_Convert(ADC_CH1); // Get 10-bit TPS position
    
    
    //Get initial block temp.
    //This is the DC and Cag offset you get while pulling...
        if(SecondsRunTime==0){
            BlockTemp_0=BlockTemp;
        
            if (BlockTemp_0 > 60) {              //hot start
                INJ_DutyCycle = DC_out[0];
                CAG_Offset = 506;        
            }
            else if (BlockTemp_0 > 50) {
                INJ_DutyCycle = 90;
                CAG_Offset = 520; 
            }
            else if (BlockTemp_0 > 40) {
                INJ_DutyCycle = 100;//190;
                CAG_Offset = 520; 
            }
            else if (BlockTemp_0 > 10) {         //room temperature start
                INJ_DutyCycle = 550;
                CAG_Offset = 560;
                TempMult = 4.0;
            } 
            else {                             //cold start
                INJ_DutyCycle = 750;
                CAG_Offset = 570;
                TempMult = 6.0;
            }
        }

    // Clear CCP4CON register and reload it with compare mode
    CCP4CON = 0x0;
    // Copy 10-bit +DC value to 16-bit CCPRxH:CCPRxL register pair
    CCPR4L = (INJ_DutyCycle << 6) & 0xFF;
    CCPR4H = (INJ_DutyCycle >> 2) & 0xFF;

    if(InjectorEnabled){
        CCP4CON = 0b00001001; // CCP1CONbits.CCP4M = ECOM_LO_MATCH, Init RD1 Hi, force Lo on compare match
        
        if(!CalibrationMode){
            INJ_StartSequence();
        }
    }
    else{
        SecondsRunTime = 0; // Fueling stopped - reset sw timer
    }
}

void INJ_StartSequence(void){

    static uint16_t interval = 0;
    interval++;
    
    
    //THIS FUNCTION HAPPENS ONCE PER SECOND
    if(interval>=62){
        
        //executed once per second
        SecondsRunTime++;
        
        //Pot Multiplier Setup
        LinearInterpInit(
        &PotInterp,
        0,75,
        1023,125,
        Q_PERCENT);

        INJ_Pot = ADC_Convert(ADC_CH7);
        PotMult = (int16_t)(LinearInterp(&PotInterp, INJ_Pot));
        PotMult=PotMult/100;
        
        //Pulse multiplier for steady state running
//        if (IGN_PulsePeriod<390U){  //faster than 5000 RPM
//        LinearInterpInit(
//        &PulseInterp,
//        390U,85,   //390U ~5000 RPM
//        280U,115,  //280U ~7000 RPM
//        Q_PERCENT);
//        
//        PulseMult = (int16_t)(LinearInterp(&PulseInterp, IGN_PulsePeriod)); 
//        PulseMult=PulseMult/100;
//        
//        }
        
        interval = 0;
    } //END OF 1Hz FUNCTION
    
        if (SecondsRunTime<start_time && SecondsRunTime>0) {
             
            StepNum = 12; 
            StepTime = 0;
            
            
            if (BlockTemp_0 > 60) {
                TempMult = 0.65;
            }
            else if (BlockTemp_0 > 30) {
                TempMult = 0.90;
            } 
            else if (BlockTemp_0 > 10) {                      
                TempMult = 3.0;    
            } 
            else {
                TempMult = 4.0; 
            }
        
            if (SecondsRunTime < 2) {
                INJ_DutyCycle = 50 * PotMult;//75*TempMult;
                CAG_Offset = 520; 
            }
            else if (SecondsRunTime < 5) {
                INJ_DutyCycle = 50 * PotMult;//60*TempMult;
                CAG_Offset = 520;
            }
            else if (SecondsRunTime < 10) {
                INJ_DutyCycle = 50 * PotMult;//40*TempMult;
                CAG_Offset = 520;
            } 
            else {
                INJ_DutyCycle = 50 * PotMult;//DC_out[0]*TempMult;
                CAG_Offset = 520;
            }   
        }

        else if (INJ_ThrottleADC<156) {  //141 for 1000
            INJ_DutyCycle=DC_out[0]-1;
        }
    
        else if (INJ_ThrottleADC<381) { //366 for 1000
            INJ_DutyCycle=(DC_out[INJ_ThrottleADC-156]); //141 for 1000s
        }
    
        else {

            LinearInterpInit(
            &TPS_InjPWMInterp,
            TPS_high[0], INJ_DutyCycleLo,
            TPS_high[1], INJ_DutyCycleHi,
            Q_PERCENT);

            INJ_DutyCycle = (int16_t)(LinearInterp(&TPS_InjPWMInterp, INJ_ThrottleADC));
        }
    
    
        //Steady state running step
        if (StepNum==12 && SecondsRunTime>start_time-1) {
        
            StepTime=1;  //keep it in this step
            INJ_DutyCycle = INJ_DutyCycle * PotMult; 
            //CAG_DISABLE;
            CAG_Offset = 506; 
            
        }           
}