/* 
 * File:   types.h
 * Author: dyulov
 *
 * Created on November 7, 2013, 11:40 AM
 */

#ifndef TYPES_H
#define	TYPES_H

#ifdef	__cplusplus
extern "C"{
#endif

struct VALID_SIGNAL{
    bool IsValid;
    int16_t Value;
};
typedef struct VALID_SIGNAL AnalogSignal_t;

#ifdef	__cplusplus
}
#endif

#endif	/* TYPES_H */

