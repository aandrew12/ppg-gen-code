/* 
 * File:   fixed_point.h
 */

#ifndef FIXED_POINT_H
#define	FIXED_POINT_H

#ifdef	__cplusplus
extern "C"{
#endif

// Fixed point format: Q[QI].[QF]->Q[6].[10] Range: 0-63.1023

#define Q_IU 10 // I for Current, U for voltage, all current and voltages are Q10
#define R_IU 1024
#define Q_TEMP 10 // Q point for temperature measurments
#define R_TEMP 1024
#define Q_ONE 14 // Q_ONE has a range -1.999 to 1.999
#define R_ONE 16384 // 2^14, the reference for a Q_ONE
#define Q_PERCENT 10
#define R_PERCENT 1024

#define V_REF 5.0
#define I_REF 100.0
#define R_REF (V_REF / I_REF * 1000) // mOhm
#define MHO_REF (I_REF / V_REF) // Siemens (1/Ohm)
#define P_REF (V_REF * I_REF)
#define TR_REF (1024 / R_REF)
#define PERCENT_REF 100.0
    
#define Q10_MUL(A,B) (int16_t)(((int32_t)((int16_t)(A))*(int32_t)((int16_t)(B)))>>10)
#define Q16U_MUL(A,B) (uint16_t)(((uint32_t)((uint16_t)(A))*(uint32_t)((uint16_t)(B)))>>16)
#define Q_MUL(A,B,Q) (int16_t)(((int32_t)((int16_t)(A))*(int32_t)((int16_t)(B)))>>(Q))
#define QU_MUL(A,B,Q) (uint16_t)(((uint32_t)((uint16_t)(A))*(uint32_t)((uint16_t)(B)))>>(Q))
#define Q_DIV(A,B,Q) (int16_t)(((int32_t)(A)<< (Q))/(int32_t)(B))
#define Q_DIV_32(A,B,Q) (int32_t)(((int32_t)(A)<< (Q))/(int32_t)(B))

/*
 * val- Real world value to be converted to PU.
 * ref- Real world value that corresponds to 1 PU.
 * Qnom- Raw value in DSP for 100% of reference.
 *  This is usually 2^Q_value for all vars
 */
#define PU_16U_CONVERT(val,ref,Qnom) (uint16_t)( ((float)(val)) / ((float)(ref)) * ((float)(Qnom)) + 0.5)
#define PU_16S_CONVERT(val,ref,Qnom) (int16_t) ( ((float)(val)) / ((float)(ref)) * ((float)(Qnom)) + ((val) > 0 ? 0.5 : -0.5) )
    
#ifdef	__cplusplus
}
#endif

#endif	/* FIXED_POINT_H */

