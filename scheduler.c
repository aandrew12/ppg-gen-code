
#include <xc.h>

#include "peripherals.h"
#include "scheduler.h"
#include "CalcFuelTypeParameter.h"

Task_t SCH_Tasks[SCH_MAX_TASKS]; // Array of tasks
uint8_t SCH_ErrorCode = 0; // Diagnostic error code
static uint16_t SCH_ErrorCount = 0;
static uint8_t SCH_ErrorLast = 0;

// Call this function first to initialize scheduler data members
void SCH_Init(void){
    uint8_t index;

    for(index = 0; index < SCH_MAX_TASKS; index++){
        SCH_DeleteTask(index);
    }
    // SCH_DeleteTask() will generate an error code once all tasks are removed
    SCH_ErrorCode = 0;
}

// Call this function to add tasks to scheduler
uint8_t SCH_AddTask(void(*pFunction)(void), const uint16_t delay, const uint16_t period){
   uint8_t index = 0;
   // Find empty slot in the array
   while ((SCH_Tasks[index].pTask != 0) && (index < SCH_MAX_TASKS)){
      index++;
   }
   // Reached end of list
   if (index == SCH_MAX_TASKS){
       // FIXME: Error handling is not implemented
      SCH_ErrorCode = ERROR_SCH_TOO_MANY_TASKS;
      return SCH_MAX_TASKS;
   }
   // Found empty slot
   SCH_Tasks[index].pTask = pFunction;
   SCH_Tasks[index].Delay = delay;
   SCH_Tasks[index].Period = period;
   SCH_Tasks[index].TaskReady = 0;
   return index; // return position of task to allow later deletion
}

uint8_t SCH_DeleteTask(const uint8_t task_index){
    uint8_t return_code;
    // FIXME: Error handling is not implemented
    if(SCH_Tasks[task_index].pTask == 0){
        SCH_ErrorCode = ERROR_SCH_CANNOT_DELETE_TASK;
        return_code = RETURN_ERROR;
    }else{
        return_code = RETURN_NORMAL;
    }
    SCH_Tasks[task_index].pTask = 0x0000;
    SCH_Tasks[task_index].Delay = 0;
    SCH_Tasks[task_index].Period = 0;
    SCH_Tasks[task_index].TaskReady = 0;

    return return_code;
}

// Call after adding all tasks and hardware initialization
void SCH_Start(){
    // Initialize and start scheduler interrupt timer
    TMR0_Init();
    // Enable interrupts
    InterruptInit();
}

// Run Tasks.
// This function is called from main loop
void SCH_DispatchTasks(void){
    uint8_t index;
    // Calls the next task if one is ready
    for(index = 0; index < SCH_MAX_TASKS; index++){
        if(SCH_Tasks[index].TaskReady > 0){
            (*SCH_Tasks[index].pTask)(); // Run the task
            SCH_Tasks[index].TaskReady -= 1; // Clear task ready flag
            // If this is a one shot task remove it from the array
            if(SCH_Tasks[index].Period == 0){
                SCH_DeleteTask(index);
            }
        }
    }
    // SCH_Idle(); TODO: Implement power saving mode
}

// Set Ready flags for tasks that need to run.
// This function is called from timer interrupt vector.
void SCH_Update(void){
    uint8_t index;
    // Calculations are in ticks
    for(index = 0; index < SCH_MAX_TASKS; index++){
        // Check if there is a task at this location
        if(SCH_Tasks[index].pTask){
            if(SCH_Tasks[index].Delay == 0){
                // The task is due to run
                SCH_Tasks[index].TaskReady += 1; // Increment flag
                if(SCH_Tasks[index].Period){
                    // Schedule periodic tasks to run again
                    SCH_Tasks[index].Delay = SCH_Tasks[index].Period - 1;
                }
            }else{
                // Not ready to run, decrement the delay
                SCH_Tasks[index].Delay--;
            }
        }
    }
}
