/* 
 * File:   atemp.h
 * Author: dyulov
 *
 * Created on November 7, 2013, 9:56 AM
 */

#ifndef ATEMP_H
#define	ATEMP_H

#ifdef	__cplusplus
extern "C"{
#endif

void AirTempInit(void);
void AirTempMonitor(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ATEMP_H */

