/*
 * File:   CalcFuelTypeParameter.c
 * Author: labrat
 *
 * Created on March 28, 2016, 11:30 AM
 */



#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>


#include <stdlib.h>
#include "fixed_point.h"
#include "peripherals.h"

#include "control.h"
#include "config.h"

#include "types.h"
#include "scheduler.h"


#include "types.h"
#include "peripherals.h"
#include "CalcFuelTypeParameter.h"



extern uint16_t TempSet=240;
extern uint16_t TempThrStep12=82;
extern uint16_t TempThrStep11=80;
extern uint16_t TempThrStep9=70;
extern uint16_t TempThrStep7=60;
int i=0;

extern double time_mult_cold[]={1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00}; 
extern double time_mult_warm[]={0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00};

extern int cag_offset_warm[]=  {506, 506, 506, 506, 506, 506, 506, 506, 506, 506, 506, 506, 506};
extern int cag_offset_cold[]=  {506, 506, 506, 506, 506, 506, 506, 506, 506, 506, 506, 506, 506};

extern FuelType_t FuelType;



void CagOffsetInit(void){
     if (FuelType==FUEL_TYPE_DSL){

       cag_offset_warm[2]=550;
       cag_offset_warm[3]=510;
       cag_offset_warm[4]=510;
       cag_offset_warm[5]=510;
       cag_offset_warm[6]=511;
       cag_offset_warm[7]=511;
       cag_offset_warm[8]=511;
       cag_offset_warm[9]=511;
       cag_offset_warm[10]=441;
       cag_offset_warm[11]=401;

       cag_offset_cold[3]=530;
       cag_offset_cold[4]=521;
       cag_offset_cold[5]=511;
       cag_offset_cold[6]=511;
       cag_offset_cold[7]=511;
       cag_offset_cold[8]=511;
       cag_offset_cold[9]=511;
       cag_offset_cold[10]=441;
       cag_offset_cold[11]=401;


    }

    else if (FuelType==FUEL_TYPE_GAS){
        cag_offset_warm[0]=200;
       cag_offset_warm[1]=200;
       cag_offset_warm[2]=200;
        cag_offset_warm[3]=200;
       cag_offset_warm[4]=200;
       cag_offset_warm[5]=200;
       cag_offset_warm[6]=200;
       cag_offset_warm[7]=200;
       cag_offset_warm[8]=200;
       cag_offset_warm[9]=200;
       cag_offset_warm[10]=200;
       cag_offset_warm[11]=200;

        cag_offset_cold[0]=350;
       cag_offset_cold[1]=350;
       cag_offset_cold[2]=350;
       cag_offset_cold[3]=200;
       cag_offset_cold[4]=200;
       cag_offset_cold[5]=200;
       cag_offset_cold[6]=200;
       cag_offset_cold[7]=200;
       cag_offset_cold[8]=200;
       cag_offset_cold[9]=200;
       cag_offset_cold[10]=200;
       cag_offset_cold[11]=200;

    }
}
void TimeMultInit(void){
    if  (FuelType==FUEL_TYPE_DSL){
        for(i=0;i<12;i++){
            time_mult_cold[i]=time_mult_cold[i];//*1.08;
            time_mult_warm[i]=time_mult_warm[i]*.9;
        }
    }

      if  (FuelType==FUEL_TYPE_GAS){

          for(i=3;i<12;i++){
            time_mult_cold[i]=1;
            time_mult_warm[i]=1;
        }
    }


}

void TempParametersInit(void){






    if  (FuelType==FUEL_TYPE_GAS){
        TempSet=260;
        TempThrStep12=70;
        TempThrStep11=60;
        TempThrStep9=52;
        TempThrStep7=45;

    }

    else if  (FuelType==FUEL_TYPE_DSL){
        TempSet=310;
        TempThrStep12=93; //93
        TempThrStep11=86;
        TempThrStep9=80;
        TempThrStep7=75;
    }
        else if  (FuelType==FUEL_TYPE_JP8){
        TempSet=278; //86
        TempThrStep12=60;
        TempThrStep11=60;
        TempThrStep9=60;
        TempThrStep7=50;  //changed 10/16/18 from 57

    }

    

    }


