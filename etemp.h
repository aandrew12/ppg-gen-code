/* 
 * File:   etemp.h
 * Author: dyulov
 *
 * Created on November 7, 2013, 11:47 AM
 */

#ifndef ETEMP_H
#define	ETEMP_H

#ifdef	__cplusplus
extern "C"{
#endif

void EngTempInit(void);
void EngTempMonitor(void);
void EngTempControl(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ETEMP_H */

