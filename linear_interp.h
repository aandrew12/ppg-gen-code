/* 
 * File:   linear_interp.h
 */

#ifndef LINEAR_INTERP_H
#define	LINEAR_INTERP_H

#ifdef	__cplusplus
extern "C"{
#endif

/*
    Perform Linear Interpolation on the linear interp structure.

    Performs a linear interpolation between (x1, y1) and (x2, y2)
    clamping the output y at the end points.

    For x1 == x2, y1 will be returned.

    Correct calibration requires that x1 <= x2. If x1 > x2, the data points will
    be swapped and a warning logged in the init function.

    If the desired ramp would cause overflow, a calibration warning will be
    logged, by the init function, and the largest possible slope will be used.
*/

struct LINEAR_INTERP_STRUCT{
    int16_t x1; // x coordinate of first data point
    int16_t y1; // y coordinate of first data point
    int16_t x2; // x coordinate of second data point
    int16_t y2; // y coordinate of second data point
    int16_t slope; // calculated in init function (y2-y1)/(x2-x1)
    int16_t q_slope;
};

/** Initialize the Linear Interp Structure

    Ensures that x1<=x2, which is required by LinearInterp().
    A calibration warning will be logged if x1 > x2 and the data points wil
    automatically be switched, assuming specified out of order.

    A calibration warning will also be logged if the desired slope
    for the ramp will cause an overflow. In this case, the largest
    possible value will be used instead, resulting in a ramp with the steepest
    possible ramp and a discontinuity at the end (to jump to the end point
    value).
*/
void LinearInterpInit(
        struct LINEAR_INTERP_STRUCT * d, // Linear Interp Structure
        int16_t x1, // x coordinate of first data point
        int16_t y1, // y coordinate of first data point
        int16_t x2, // x coordinate of second data point
        int16_t y2, // y coordinate of second data point
        int16_t q_slope // Q value for intermediate slope value (deltaY/deltaX)
);

/** Perform Linear Interpolation on the linear interp structure.

    Performs a linear interpolation between (x1, y1) and (x2, y2)
    clamping the output y at the end points.
    Requires that x1 <= x2 (enforced in init function).

    For x1 == x2, y1 will be returned.

    @return y, the interpolated output value.
*/
int16_t LinearInterp(
        struct LINEAR_INTERP_STRUCT * d, // Linear Interp Structure
        int16_t x // current value of x
);


#ifdef	__cplusplus
}
#endif

#endif	/* LINEAR_INTERP_H */

