/*
 * File:   dac.c
 * Author: dyulov
 *
 * Created on August 1, 2013, 5:17 PM
 */
#define USE_OR_MASKS
#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>

#include "peripherals.h"
#include "config.h"
#include "dac.h"


// DAC SPI configuration bits
#define DAC_A 0b00000000 // Select channel A. Bit 15
#define DAC_B 0b10000000 // Select channel B. Bit 15
#define DAC_BUF_OFF 0b00000000 // Vref In Unbuffered - 165K, 7pF. Bit 14
#define DAC_BUF_ON 0b10000000 // Vref In Buffered - High Impedance. Bit 14
#define DAC_GA_1X 0b00100000 // Output gain x1 2.048V (VOUT = VREF * D/4096). Bit 13
#define DAC_GA_2X 0b00000000 // Output gain x2 4.096V (VOUT = 2 * VREF * D/4096) VREF = 2.048V. Bit 13
#define DAC_CH_ACTIVE   0b00010000 // Output of the selected ch is active. Bit 12
#define DAC_CH_SHDN     0b00000000 // Output of the selected ch is shut down. Bit 12

#if (DAC_TYPE == DAC_MCP4802)
uint8_t CAG_Offset = 155; // code 155 = 2.5V = 0 offset

/*
 * TODO: extract into 3 functions DAC_Write(), DAC_CAG_Offet(), DAC_PumpSpeed()
 * Vout = ((2.048 * Dn)/(2^n)) * G
 * Range 0.0V to 255/256 * 4.096V (G = 2)
 * Lsb = 4.096V/256 = 16 mV
 */
void DAC_Write(void){
    uint8_t config = 0x00; // 4 MSB bits [15:13] are config bits
    uint8_t pump_setpoint = 0xFF;  // 8 bits [11:4] data + 4 bits unused
    uint8_t data_out_msb = 0x00;
    uint8_t data_out_lsb = 0x00;

    config = DAC_B | DAC_GA_2X | DAC_CH_ACTIVE;
    data_out_msb = config | (CAG_Offset >> 4);
    data_out_lsb = CAG_Offset << 4;

    SPI_SS_DAC_ON;
    WriteSPI1(data_out_msb); // This will result in one low clock bit between
    WriteSPI1(data_out_lsb); // two bytes but this approach can still be used
    SPI_SS_DAC_OFF;

    // 4V output is insufficient to control pump speed therefore following code
    // is not used until OPAMP is added to PCB.
#if 0
    config = 0x00; // 4 MSB bits [15:13] are config bits
    data_out_msb = 0x00;
    data_out_lsb = 0x00;
    config = DAC_A | DAC_GA_X2 | DAC_CH_SHDN; // output not used - shutdown
    data_out_msb = config | (pump_setpoint >> 4);
    data_out_lsb = pump_setpoint << 4;

    SPI_SS_DAC_ON;
    WriteSPI1(data_out_msb); // This will result in one low clock bit between
    WriteSPI1(data_out_lsb); // two bytes but this approach can still be used
    SPI_SS_DAC_OFF;
#endif
}
#elif (DAC_TYPE == DAC_MCP4912)
uint16_t CAG_Offset = 511; // code 511 = 2.5V = 0 offset


#define DAC_RES 1023 // 2**10|2**8|2**12
#define DAC_VREF 5.0 // V
//#define DAC_VREF 3.0 // V 3 v for 2 kw!

#define DAC_GAIN 1 // x1|x2
#if (ENGINE_NUMBER > 2000)
#define DAC_VOUT 3.0 // 2KW engines
#else
#define DAC_VOUT 5.0 // 1KW engines
#endif
//#define DAC_CODE (uint16_t)(DAC_VOUT*DAC_RES/DAC_VREF/DAC_GAIN)
#define DAC_CODE (uint16_t)(DAC_VOUT*DAC_RES/DAC_VREF/DAC_GAIN)

uint16_t DAC_Code = DAC_CODE;
/*
 * TODO: extract into 3 functions DAC_Write(), DAC_CAG_Offet(), DAC_PumpSpeed()
 * Vout = ((2.048 * Dn)/(2^n)) * G
 * Range 0.0V to 255/256 * 4.096V (G = 2)
 * Lsb = 4.096V/256 = 16 mV
 */
void DAC_Write(void){
    uint8_t config = 0x00; // 4 MSB bits [15:12] are config bits
    uint16_t pump_setpoint = DAC_Code;  // 10 bits [11:2] data + bits [1:0] unused
    uint8_t data_out_msb = 0x00;
    uint8_t data_out_lsb = 0x00;

    config = DAC_B | DAC_BUF_OFF | DAC_GA_1X | DAC_CH_ACTIVE;
    data_out_msb = config | ((CAG_Offset >> 6) & 0xF);
    data_out_lsb = (CAG_Offset << 2) & 0xFF;

    SPI_SS_DAC_ON;
    WriteSPI1(data_out_msb); // This will result in one low clock bit between
    WriteSPI1(data_out_lsb); // two bytes but this approach can still be used
    SPI_SS_DAC_OFF;

    // Set analog input to KNF pump
    config = 0x00; // 4 MSB bits [15:12] are config bits
    data_out_msb = 0x00;
    data_out_lsb = 0x00;
    config = DAC_A | DAC_BUF_OFF | DAC_GA_1X | DAC_CH_ACTIVE;
    data_out_msb = config | (pump_setpoint >> 6);
    data_out_lsb = (pump_setpoint << 2) & 0xFF;

    SPI_SS_DAC_ON;
    WriteSPI1(data_out_msb); // This will result in one low clock bit between
    WriteSPI1(data_out_lsb); // two bytes but this approach can still be used
    SPI_SS_DAC_OFF;
}
#endif
