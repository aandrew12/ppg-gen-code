#ifndef PERIPHERALS_H
#define	PERIPHERALS_H
// Standard library headers
#include <stdint.h>
// Peripheral library headers
#include <adc.h>

#define PIN_LOW 0
#define PIN_HIGH 1
#define PIN_OUTPUT 0
#define PIN_INPUT 1

#define TMR0_VALUE 49536 // 1 ms interrupt at 64 MHz w/prescaler = 1
#define TMR1_VALUE 0x0 // Sets PWM period when used w/CCP1 in compare mode
/* Digital outputs */
#define LSD_SPARE_ENABLE do {LATDbits.LATD0 = 1;} while(0)
#define LSD_SPARE_DISABLE do {LATDbits.LATD0 = 0;} while(0)
// Open/Close injector - only used during intitialization
#define LSD_INJECTOR_ON do {LATDbits.LATD1 = 1;} while(0) // [39]RD1/CCP4
#define LSD_INJECTOR_OFF do {LATDbits.LATD1 = 0;} while(0) 
// Enable/disable fuel pump motor. Active high
#define FUEL_PUMP_ENABLE do {LATDbits.LATD2 = 1;} while(0)
#define FUEL_PUMP_DISABLE do {LATDbits.LATD2 = 0;} while(0)
// Enable/disable oil pump. Active high
#define OIL_PUMP_ENABLE do {LATDbits.LATD3 = 1;} while(0)
#define OIL_PUMP_DISABLE do {LATDbits.LATD3 = 0;} while(0)
// Enable/disable LSD IC outputs. Active low
#define LSD_ENABLE do {LATDbits.LATD4 = 0;} while(0)
#define LSD_DISABLE do {LATDbits.LATD4 = 1;} while(0)
// Enable/disable CAG PCB 5V PS. Active high
#define CAG_ENABLE do {LATDbits.LATD5 = 1;} while(0)
#define CAG_DISABLE do {LATDbits.LATD5 = 0;} while(0)
// Connect/disconnect charger. Active high
#define CHRG_CONNECT do {LATDbits.LATD6 = 1;} while(0)
#define CHRG_DISCONNECT do {LATDbits.LATD6 = 0;} while(0)
#define CHRG_SWITCH_STATE (bool)(LATDbits.LATD6)
// Enable/disable 5V power latch
#define PWR_HOLD_ENABLE do {LATDbits.LATD7 = 0;} while(0)
#define PWR_HOLD_DISABLE do {LATDbits.LATD7 = 1;} while(0)
// Wait to start indicator LED. Active low
#define LED_WAIT_ON do {LATBbits.LATB0 = 0;} while(0)
#define LED_WAIT_OFF do {LATBbits.LATB0 = 1;} while(0)
#define LED_WAIT_BLINK do {LATBbits.LATB0 = ~LATBbits.LATB0;} while(0)
// Ready indicator LED. Active low
#define LED_READY_ON do {LATBbits.LATB1 = 0;} while(0)
#define LED_READY_OFF do {LATBbits.LATB1 = 1;} while(0)
// Fault indicator LED. Active low
#define LED_FAULT_ON do {LATBbits.LATB2 = 0;} while(0)
#define LED_FAULT_OFF do {LATBbits.LATB2 = 1;} while(0)
#define SPI_SS_DAC_ON do {LATBbits.LATB4 = 0;} while(0) // [14]RB4
#define SPI_SS_DAC_OFF do {LATBbits.LATB4 = 1;} while(0) 
#define SPI_SS_LSD_ON do {LATBbits.LATB3 = 0;} while(0) // [11]RB3
#define SPI_SS_LSD_OFF do {LATBbits.LATB3 = 1;} while(0) 

#define SPI1 1
#define SPI2 2
#define SPI_COMM_READY      0
#define SPI_COMM_TX_BUSY    1
#define SPI_COMM_RX_OK      2
/******************************************************************************/
/* Public Function Prototypes                                                   */
/******************************************************************************/

void InitApp(void);         /* I/O and Peripheral Initialization */
uint16_t ADC_Convert(uint8_t chan);
void PWM_HS_Update(uint8_t dutyCycle);
void PWM_LS_Init(void);
void TMR0_Init(void);
void TMR1_Init(void);
void TMR3_Init(void); // TODO: Consider moving these prototypes to c file
void InterruptInit(void);
void SPI_DAC_Write(void);
uint8_t SPI_Write16(uint16_t data, uint8_t chan);
uint16_t SPI_Read16(uint8_t chan);

#endif /* PERIPHERALS_H */