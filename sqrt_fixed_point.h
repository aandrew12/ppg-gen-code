/* 
 * File:   sqrt_fixed_point.h
 * Author: dyulov
 *
 * Created on July 24, 2013, 3:22 PM
 */

#ifndef SQRT_FIXED_POINT_H
#define	SQRT_FIXED_POINT_H

#ifdef	__cplusplus
extern "C"{
#endif

int16_t SQRT_Q10(int16_t A);

#ifdef	__cplusplus
}
#endif

#endif	/* SQRT_FIXED_POINT_H */

