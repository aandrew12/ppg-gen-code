#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>

#include "peripherals.h"
#include "scheduler.h"
#include "control.h"
#include "injector.h"
/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/
void InterruptServiceHigh(void);
void InterruptServiceLow(void);

/* High-priority service */

#if defined(__XC) || defined(HI_TECH_C)
void interrupt high_isr(void)
#elif defined (__18CXX)
#pragma code high_isr=0x08
#pragma interrupt high_isr
void high_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{

    /* This code stub shows general interrupt handling.  Note that these
    conditional statements are not handled within 3 separate if blocks.
    Do not use a seperate if block for each interrupt flag to avoid run
    time errors. */

    /* TODO Add High Priority interrupt routine code here. */

    /* Determine which flag generated the interrupt */
    if(PIR1bits.CCP1IF){ // Capture 2 interrupt - engine speed pulse up to 83.333 Hz
        GetIgnitionPeriod(); // Calculate pulse period
        PIR1bits.CCP1IF = 0; // Clear CCP1 interrupt flag
    }
    if(PIR1bits.TMR1IF){ // Timer1 interrupt 60 Hz
        INJ_SetDutyCycle(); // Injector PWM
        PIR1bits.TMR1IF = 0; // Clear TMR1 interrup flag
    }
#if 0
    else if(INTCONbits.TMR0IF){ // Timer0 interrupt
        INTCONbits.TMR0IF = 0; /* Clear Interrupt Flag */
        // Set timer TMR0 to interrupt at 1 KHz (1 ms)
        TMR0H = (TMR0_VALUE >> 8) & 0xFF; // Always write upper byte first
        TMR0L = TMR0_VALUE & 0xFF;
        SCH_Update(); // Drive scheduler

        // RS232_Write(); //User_Timer(); //
        // LATDbits.LATD7 = ~LATDbits.LATD7; // FIXME: remove
    }
    else if(<Interrupt Flag 2 >){
        <Interrupt Flag 2 = 0 >; /* Clear Interrupt Flag 2 */
    }else{
        /* Unhandled interrupts */
    }

#endif

}

/* Low-priority interrupt routine */
#if defined(__XC) || defined(HI_TECH_C)
void low_priority interrupt low_isr(void)
#elif defined (__18CXX)
#pragma code low_isr=0x18
#pragma interruptlow low_isr
void low_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{

    /* This code stub shows general interrupt handling.  Note that these
    conditional statements are not handled within 3 seperate if blocks.
    Do not use a seperate if block for each interrupt flag to avoid run
    time errors. */

    /* Determine which flag generated the interrupt */
    if(INTCONbits.TMR0IF){ // Timer0 interrupt
        INTCONbits.TMR0IF = 0; /* Clear Interrupt Flag */
        // Set timer TMR0 to interrupt at 1 KHz (1 ms)
        TMR0H = (TMR0_VALUE >> 8) & 0xFF; // Always write upper byte first
        TMR0L = TMR0_VALUE & 0xFF;
        SCH_Update(); // Drive scheduler
    }
#if 0
    if(<Interrupt Flag 2 >){
        <Interrupt Flag 1 = 0 >; /* Clear Interrupt Flag 1 */
    }else if(<Interrupt Flag 2 >){
        <Interrupt Flag 2 = 0 >; /* Clear Interrupt Flag 2 */
    }else{
        /* Unhandled interrupts */
    }

#endif

}
