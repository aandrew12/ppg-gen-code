#define USE_OR_MASKS
#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>

#include "system.h"
#include "peripherals.h"
#include "scheduler.h"
#include "injector.h"
#include "calcfueltypeparameter.h"

void main(void){
    /* Configure the oscillator for the device */
    ConfigureOscillator();
    /* Initialize I/O and Peripherals for application */
    InitApp();
    
    while(1){
        SCH_DispatchTasks();
    }
}

