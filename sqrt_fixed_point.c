// Standard library headers
#include <stdint.h>

#include "fixed_point.h"
#include "sqrt_fixed_point.h"

#define QNORM 15 // Input is normalyzed to Q15 between 0.5 ~1.

int16_t SqrtTableQ10[16] = {
    0x16A1, 0x1000, 0x0B50, 0x0800, 0x05A8,
    0x0400, 0x02D4, 0x0200, 0x016A, 0x0100,
    0x00B5, 0x0080, 0x005A, 0x0040, 0x002D,
    0};

// Polynomial coefficients
int16_t SqrtQ10PolyB3=0x1118;   // 0.267031302 as Q14
int16_t SqrtQ10PolyB2=0x45e3;   // 1.092058465 as Q14
int16_t SqrtQ10PolyB1=0xE0D1;   // -0.487314809 as Q14
int16_t SqrtQ10PolyB0=0x0835;   // 0.128256439 as Q14

// Input must be between 0 and 32768
int16_t SQRT_Q10(int16_t A){
    int16_t normA;  //Normalized A 0.5~1 * 2^15
    uint16_t index = 0;
    uint16_t root;
    int16_t rooti;
    int16_t exp;

    if(A > 0){
        if(A >= 0x4000){
            index = 0;
            normA = A;
        }else{
            if(A >= 0x2000){index = 1;
            }else if(A >= 0x1000){index = 2;
            }else if(A >= 0x0800){index = 3;
            }else if(A >= 0x0400){index = 4;
            }else if(A >= 0x0200){index = 5;
            }else if(A >= 0x0100){index = 6;
            }else if(A >= 0x0080){index = 7;
            }else if(A >= 0x0040){index = 8;
            }else if(A >= 0x0020){index = 9;
            }else if(A >= 0x0010){index = 10;
            }else if(A >= 0x0008){index = 11;
            }else if(A >= 0x0004){index = 12;
            }else if(A >= 0x0002){index = 13;
            }else{index = 14;}
            normA = A<<(index);
        }
        // Calculate polynomial approximation of square root.
        rooti = Q_MUL(normA,SqrtQ10PolyB0,QNORM) + SqrtQ10PolyB1;
        rooti = Q_MUL(normA,rooti,QNORM) + SqrtQ10PolyB2;
        rooti = Q_MUL(normA,rooti,QNORM) + SqrtQ10PolyB3;

        exp = SqrtTableQ10[index]; // Denormalyze
        root = Q_MUL(exp,rooti,QNORM-1);
   }else{
        root = 0;
   }
   return (root);
}
