/*
 * File:   etemp.h
 * Author: dyulov
 *
 * Created on November 7, 2013, 11:47 AM
 *
 * Engine Temperature Monitoring
 */

// TODO: Add better signal validation
#include <xc.h>
// Standard library headersb
#include <stdint.h>
#include <stdbool.h>
#include "types.h"
#include "peripherals.h"
#include "etemp.h"
#include "CalcFuelTypeParameter.h"

#define ETEMP_BUFFER_SIZE 16
extern uint16_t SecondsRunTime;
extern uint16_t TimeStep;
AnalogSignal_t EngTempADC_Filtered;
// AnalogSignal_t Tps_Filtered;

uint16_t EngTempDataBuffer[ETEMP_BUFFER_SIZE];
// uint16_t TpsDataBuffer[ETEMP_BUFFER_SIZE];
uint8_t EngTempBufferCounter = 0;
// uint8_t TpsBufferCounter=0;
extern uint16_t TempSet;
int testflag=2;
int overheat_flag=0;
int underheat_flag=0;
int short_flag_cold=0;
int short_flag_hot=0;
int counter = 0;
int shutter_counter=0;
int EngTemp=0;
int AbsEngTempError=0;
int n=0;
int SS_time=240;

void EngTempFilter(void);

void EngTempInit(void){
    uint8_t bufferIndex;
    SS_time=TimeStep*10;
    for(bufferIndex = 0; bufferIndex < ETEMP_BUFFER_SIZE; bufferIndex++){
        EngTempDataBuffer[bufferIndex] = 0;
    }
    EngTempADC_Filtered.Value = 0;
    EngTempADC_Filtered.IsValid = false;
}

void EngTempMonitor(void){
    EngTempDataBuffer[EngTempBufferCounter] = ADC_Convert(ADC_CH3); // Get air temperature
    EngTempBufferCounter++;
    if(EngTempBufferCounter == ETEMP_BUFFER_SIZE){
        EngTempBufferCounter = 0;
        EngTempFilter();
    }

   }

void EngTempControl(void){
    
    EngTemp=EngTempADC_Filtered.Value;
    
    if (EngTemp > 290) {  //290 ADC corresponds to 92 degC
        LSD_SPARE_ENABLE;
        OIL_PUMP_ENABLE;
    }
    else {
        LSD_SPARE_DISABLE;
        OIL_PUMP_DISABLE;
    }
    
}
void EngTempFilter(void){
    uint8_t bufferIndex;
    uint16_t engTempAvg = 0;
   // uint16_t TpsAvg =0;
    for(bufferIndex = 0; bufferIndex < ETEMP_BUFFER_SIZE; bufferIndex++){
        engTempAvg += EngTempDataBuffer[bufferIndex];
      //  TpsAvg+=TpsDataBuffer[bufferIndex];
    }
    engTempAvg = engTempAvg >> 4;
   // TpsAvg = TpsAvg >> 4;
   // Tps_Filtered.Value=TpsAvg;
    EngTempADC_Filtered.Value = engTempAvg;
    EngTempADC_Filtered.IsValid = true;
   // Tps_Filtered.IsValid=true;
}

