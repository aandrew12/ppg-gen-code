/* 
 * File:   injector.h
 * Author: dyulov
 *
 * Created on April 17, 2013, 12:01 AM
 */

#ifndef INJECTOR_H
#define	INJECTOR_H

#ifdef	__cplusplus
extern "C"{
#endif

void RecalibrateTPS(void);
void INJ_Init(void);
void INJ_SetDutyCycle(void);
void INJ_FuelModifierCalc(void);

#ifdef	__cplusplus
}
#endif

#endif	/* INJECTOR_H */

