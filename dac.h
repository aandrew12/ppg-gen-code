/* 
 * File:   dac.h
 * Author: dyulov
 *
 * Created on August 1, 2013, 5:17 PM
 */

#ifndef DAC_H
#define	DAC_H

#ifdef	__cplusplus
extern "C"{
#endif

#define DAC_MCP4802 0 // 8-bit, dual, internal VREF = 2.048V
#define DAC_MCP4912 1 // 10-bit, dual, external VREF = 5V
#define DAC_TYPE DAC_MCP4912

void DAC_Write(void);

#ifdef	__cplusplus
}
#endif

#endif	/* DAC_H */

