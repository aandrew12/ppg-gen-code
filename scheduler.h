
#ifndef SCHEDULER_H
#define	SCHEDULER_H

#define RETURN_NORMAL (0)
#define RETURN_ERROR (1)

#define ERROR_SCH_TOO_MANY_TASKS (1)
#define ERROR_SCH_CANNOT_DELETE_TASK (2)

#define SCH_MAX_TASKS (4)
#define SCH_ACTIVE (1)

typedef struct{
   void (*pTask)(void); // Pointer to the Task function
   uint16_t Delay; // Number of ticks before first call
   uint16_t Period; // Number of ticks between calls
   uint8_t TaskReady; // Flag set by scheduler
}Task_t;

// Scheduler interface functions
void SCH_Init(void);
uint8_t SCH_AddTask(void(*pFunction)(void), const uint16_t delay, const uint16_t period); // Call from main()
uint8_t SCH_DeleteTask(const uint8_t task_index);
void SCH_Start(void); // Call from main()
void SCH_DispatchTasks(void); // Call from main()
void SCH_Update(void); // Call from timer interrrupt

#endif	/* SCHEDULER_H */

