/*
 * File:   control.c
 * Author: dyulov
 *
 * Created on July 25, 2013, 3:30 PM
 *
 * State Machine implementation
 */
#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>

#include "peripherals.h"
#include "dac.h"
#include "system.h"
#include "injector.h"
#include "types.h"
#include "atemp.h"
#include "etemp.h"
#include "control.h"
#include "calcfueltypeparameter.h"

#define NO_ENGINE_INJECTOR_TEST 0 // Must be 0 for normal operation
#if (DAC_TYPE == DAC_MCP4802)
#define CAG_PERCENT_ALCOHOL 127 // 8-bit 127 = 90%, 136 = 93%, 156 = 100%
#elif (DAC_TYPE == DAC_MCP4912)
#define CAG_PERCENT_ALCOHOL 420 // 10-bit 420 = 90%, 511 = 100%
#endif

extern uint16_t CAG_Offset;
extern AnalogSignal_t AirTempADC_Filtered;
extern AnalogSignal_t EngTempADC_Filtered;
extern float BlockTemp = 20.0;


/* File Scope Variables */
// FSM from dmitry
GenCtrlState_t GenCtrlState = GEN_STATE_INIT;
uint8_t GenCtrlFaultCode = 0;
uint16_t TimerInState = 0;
uint16_t UI_InputADC = 0; // UI analog input - fuel type and drain
bool DrainMode = false;
// Engine speed aka ignition pulse input 1 per revolution
uint16_t IGN_PulseTimeOld = 0;
uint16_t IGN_PulseTimeNew = 0;
uint16_t IGN_PulsePeriod = 0;
uint8_t IGN_PulseTimeoutCntr = 0;
uint16_t TimeStep = 30;
uint16_t TimeWait = 1500; //15 seconds, 100 ticks per second

// External interface
bool InjectorEnabled = false;
bool ChargeEnabled = false;
extern FuelType_t FuelType = FUEL_TYPE_FLT;
bool CalibrationMode = false;

/* File Scope Prototypes */
static void EnterGenStateInit(void);
static void ExecuteGenStateInit(void);
static void EnterGenStateDrain(void);
static void ExecuteGenStateDrain(void);
static void EnterGenStateWait(void);
static void ExecuteGenStateWait(void);
static void EnterGenStateStart(void);
static void ExecuteGenStateStart(void);
static void EnterGenStateRun(void);
static void ExecuteGenStateRun(void);
static void EnterGenStateOff(void);
static void ExecuteGenStateOff(void);
static void EnterGenStateCal(void);
static void ExecuteGenStateCal(void);
void GenCtrlFSM(void);
void BDCFuelPumpEnable(void);
void LSD_FaultCommand(void);
void BlockTempToTimeInit(void);

void GenControlInit(void){
  
     AirTempInit();
    EngTempInit();
    EnterGenStateInit();
   
}

void GenControl(void){
    AirTempMonitor();
    EngTempMonitor();
    GenCtrlFSM();
#if NO_ENGINE_INJECTOR_TEST // Used for injector testing without engine
   
#endif
    
    if(IGN_PulseTimeoutCntr){IGN_PulseTimeoutCntr--;}
}

static void EnterGenStateInit(void){
    LSD_FaultCommand(); // Setup fault LSD fault handling
    LSD_DISABLE;
    FUEL_PUMP_DISABLE;
    CAG_DISABLE;
    InjectorEnabled = false;
    ChargeEnabled = false;
    GenCtrlState = GEN_STATE_INIT;
}

static void ExecuteGenStateInit(void){
   
     if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    //if((!AirTempADC_Filtered.IsValid) || (!EngTempADC_Filtered.IsValid)){
     if(!EngTempADC_Filtered.IsValid){
        return;
    }
    
    FuelType = FUEL_TYPE_JP8; 
    
    CagOffsetInit();
    TimeMultInit();
    TempParametersInit();
    
    // Get state of the Drain switch
    if(DrainMode){
        EnterGenStateDrain();
    }else{
        BlockTempToTimeInit();
        EnterGenStateWait();
    }


}

static void EnterGenStateDrain(void){
    LSD_ENABLE;
    FUEL_PUMP_ENABLE;
    BDCFuelPumpEnable();
    LED_WAIT_ON;
    GenCtrlState = GEN_STATE_DRAIN;
    TimerInState = TMR_DRAIN_TIMEOUT;
}

static void ExecuteGenStateDrain(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    if(!TimerInState--){
        EnterGenStateOff();
        return;
    }
}

static void EnterGenStateWait(void){
    LSD_ENABLE;
    FUEL_PUMP_ENABLE;
    BDCFuelPumpEnable();
  //  if(FuelType != FUEL_TYPE_GAS){
        CAG_ENABLE;
   // }
        if (FuelType==FUEL_TYPE_GAS){
                CAG_Offset=10;
            //CAG_DISABLE;
        }
    GenCtrlState = GEN_STATE_WAIT;
   // TimerInState = TMR_WAIT_TIMEOUT; // TODO: Should be temp compensated
    TimerInState = TimeWait;
}

static void ExecuteGenStateWait(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    if(!(TimerInState%50))LED_WAIT_BLINK;
    if((!TimerInState--)||IGN_PulseTimeoutCntr){
        EnterGenStateStart();
        return;
    }
}

static void EnterGenStateStart(void){
    ChargeEnabled = true;
    LED_WAIT_OFF;
    LED_READY_ON;

    GenCtrlState = GEN_STATE_START;
    TimerInState = TMR_START_TIMEOUT;
   
    INJ_Init();
}

static void ExecuteGenStateStart(void){
    
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    // Run injector if ignition pulse is present
    if(IGN_PulseTimeoutCntr){
        InjectorEnabled = true;
     
        TimerInState = TMR_START_TIMEOUT; // Reset in-state timeout
    }else{
        InjectorEnabled = false;
    }
    // Monitor engine speed and transition to RUN if above threshold
    if((IGN_PulsePeriod > IGN_PULSE_PERIOD_MIN) && (IGN_PulsePeriod < IGN_PULSE_PERIOD_RUN)){
        EnterGenStateRun();
        return;
    }
    // Turn OFF if engine is not started within specified time
    if(!TimerInState--){
        EnterGenStateOff();
        return;
    }
}

static void EnterGenStateRun(void){
    InjectorEnabled = true;
    GenCtrlState = GEN_STATE_RUN;
   
}

static void ExecuteGenStateRun(void){
    
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    
    // Monitor ignition pulse and transition to OFF if not detected within predetermined period of time
    if(!IGN_PulseTimeoutCntr){
        EnterGenStateOff();
        return;
    }
}

static void EnterGenStateOff(void){
    CAG_DISABLE;
    InjectorEnabled = false;
    FUEL_PUMP_DISABLE;
    LSD_DISABLE;
    ChargeEnabled = true; // TODO: This is to allow external charging until "charger on" input is available
    LED_WAIT_OFF;
    LED_READY_OFF;
    LED_FAULT_ON;
    GenCtrlState = GEN_STATE_OFF;
}

static void ExecuteGenStateOff(void){
    ; // Stay here until reset
}

static void EnterGenStateCal(void){
    LSD_ENABLE;
    FUEL_PUMP_ENABLE;
    BDCFuelPumpEnable();
    RecalibrateTPS();
    InjectorEnabled = true;
    CAG_ENABLE;
    LED_WAIT_ON;
    LED_READY_ON;
    CalibrationMode = true;
    GenCtrlState = GEN_STATE_CAL;
}

static void ExecuteGenStateCal(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
}

void GenCtrlFSM(void){
    switch (GenCtrlState) {
        case GEN_STATE_INIT:
            ExecuteGenStateInit();
            break;
        case GEN_STATE_DRAIN:
            ExecuteGenStateDrain();
            break;
        case GEN_STATE_WAIT:
            
     
            
            ExecuteGenStateWait();

            break;
        case GEN_STATE_START:
             
           

            ExecuteGenStateStart();
            break;
        case GEN_STATE_RUN:
            ExecuteGenStateRun();
            break;
        case GEN_STATE_CAL:
            ExecuteGenStateCal();
            break;
        case GEN_STATE_OFF:
            ExecuteGenStateOff();
            break;
        default:
            ExecuteGenStateOff();
            break;
    }
}

/* Called from high priority ISR */
void GetIgnitionPeriod(void){
  
    IGN_PulseTimeNew = ReadECapture1();
    if(IGN_PulseTimeOld > 0){
        IGN_PulsePeriod = IGN_PulseTimeNew - IGN_PulseTimeOld;
    }
    IGN_PulseTimeOld = IGN_PulseTimeNew;
    
    if((IGN_PulsePeriod > IGN_PULSE_PERIOD_MIN) && (IGN_PulsePeriod < IGN_PULSE_PERIOD_MAX)){ // up to 125 mS
 
        IGN_PulseTimeoutCntr = TMR_IGN_PULSE_TIMEOUT;
    }else{
        IGN_PulsePeriod = 0;
    }
}


void BDCFuelPumpEnable(void){
   
    uint16_t PWM_FrequencyDytyCycle = 0b1010011110100001; // 33% 20*0.25 = 5V
    uint16_t PWM_Enable = 0b0001111100001111;
    uint16_t PWM_Output = 0b0011000000100000;

    SPI_SS_LSD_ON;
    SPI_Write16(PWM_FrequencyDytyCycle, SPI1);
    SPI_SS_LSD_OFF;

    __delay_us(100);
    SPI_SS_LSD_ON;
    SPI_Write16(PWM_Enable, SPI1);
    SPI_SS_LSD_OFF;

    __delay_us(100);
    SPI_SS_LSD_ON;
    SPI_Write16(PWM_Output, SPI1);
    SPI_SS_LSD_OFF;

  
}

void LSD_FaultCommand(void){
    
    uint16_t faultCommand = 0b0010110000000000;

    SPI_SS_LSD_ON;
    SPI_Write16(faultCommand, SPI1);
    SPI_SS_LSD_OFF;
}

void BlockTempToTimeInit(void){
      
   BlockTemp = EngTempADC_Filtered.Value * .4817 - 48.7; //DEG C

    if(BlockTemp < -15.0){ //
        TimeStep = 60;
        TimeWait = 600u; //6 seconds
    }else if(BlockTemp < -10.0){ //<-10 degrees  C
        TimeStep = 60;
        TimeWait = 600u; //6 seconds
    }else if(BlockTemp < -5.0){ //<-5 degrees  C
        TimeStep = 55;
        TimeWait = 600u; //6 seconds
    }else if(BlockTemp < 0.0){ //<0 degrees  C
        TimeStep = 50;

        TimeWait = 500u; //5 seconds
    }else if(BlockTemp < 5.0){ //<5 degrees  C
        TimeStep = 45;
        TimeWait = 500u; //5 seconds
    }else if(BlockTemp < 15.0){ //< 15 degrees
        TimeStep = 30;
        TimeWait = 500u; //5 seconds
    }else if(BlockTemp < 20.0){ //< 20 degrees
        TimeStep = 25;
        TimeWait = 500u; //5 seconds
    }else if(BlockTemp < 30.0){ //< 30 degrees
        TimeStep = 15;
        TimeWait = 400u; //4 seconds
    }else if(BlockTemp < 40.0){ //<~40 degrees
        TimeStep = 9;
        TimeWait = 300u; //3 seconds
    }else if(BlockTemp < 50.0){ //<~50 degrees
        TimeStep = 8;
        TimeWait = 200u; //2 seconds
    }else if(BlockTemp < 65.0){ //~60 degrees in 9fa
        TimeStep = 6;
        TimeWait = 100u; //1 seconds
    }else if(BlockTemp < 80.0){ // 70 degrees  in 9fa
      
        TimeStep=2;
        TimeWait = 10u; //.1 seconds
    }else{ //above 80 degrees
       
         TimeStep=1;
        TimeWait = 10u; //.1 seconds
        
    }
   TimeStep=10;
  
  
    if(FuelType == FUEL_TYPE_GAS){
        TimeStep = 1; // FIXME: This doesn't work, convert time to fp or eliminate
        TimeWait = 50u;
    }

   if(FuelType == FUEL_TYPE_DSL){
      
        TimeWait = 800u;
    }
}
