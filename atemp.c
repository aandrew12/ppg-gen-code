/*
 * File:   atemp.h
 * Author: dyulov
 *
 * Created on November 7, 2013, 9:56 AM
 *
 * Intake Air Temperature Monitoring
 */
#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>

#include "types.h"
#include "peripherals.h"
#include "atemp.h"

#define ATEMP_BUFFER_SIZE 16
AnalogSignal_t AirTempADC_Filtered;
uint16_t AirTempDataBuffer[ATEMP_BUFFER_SIZE];
uint8_t AirTempBufferCounter = 0;

void AirTempFilter(void);

void AirTempInit(void){
    uint8_t bufferIndex;
    for(bufferIndex = 0; bufferIndex < ATEMP_BUFFER_SIZE; bufferIndex++){
        AirTempDataBuffer[bufferIndex] = 0;
    }
    AirTempADC_Filtered.Value = 0;
    AirTempADC_Filtered.IsValid = false;
}

void AirTempMonitor(void){
    AirTempDataBuffer[AirTempBufferCounter] = ADC_Convert(ADC_CH7); // Get air temperature
    AirTempBufferCounter++;
    if(AirTempBufferCounter == ATEMP_BUFFER_SIZE){
        AirTempBufferCounter = 0;
        AirTempFilter();
    }
}

void AirTempFilter(void){
    uint8_t bufferIndex;
    uint16_t airTempAvg = 0;
    for(bufferIndex = 0; bufferIndex < ATEMP_BUFFER_SIZE; bufferIndex++){
        airTempAvg += AirTempDataBuffer[bufferIndex];
    }
    airTempAvg = airTempAvg >> 4;
    AirTempADC_Filtered.Value = airTempAvg;
    AirTempADC_Filtered.IsValid = true;
}
