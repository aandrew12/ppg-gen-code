/* 
 * File:   battery.h
 * Author: dyulov
 *
 * Created on July 29, 2013, 10:27 AM
 */

#ifndef BATTERY_H
#define	BATTERY_H

#ifdef	__cplusplus
extern "C"{
#endif
    
void BatteryMonitorInit(void);
void BatteryMonitor(void);

#ifdef	__cplusplus
}
#endif

#endif	/* BATTERY_H */

