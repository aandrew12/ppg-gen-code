/******************************************************************************/
/* System Level #define Macros                                                */
/******************************************************************************/

/* TODO Define system operating frequency */

/* Microcontroller MIPs (FCY) */
#define SYS_FREQ        64000000L
#define FCY             SYS_FREQ/4
#define _XTAL_FREQ SYS_FREQ
#define TICKS_1US FCY/1000000

typedef enum { C_250kHz = 0,
               C_500kHz = 1,
               C_1MHz = 2,
               C_2MHz = 3,
               C_4MHz = 4,
               C_8MHz = 5,
               C_16MHz = 6,
               C_32MHz = 7,
               C_64MHz = 8} IntOSCFreq;
/******************************************************************************/
/* System Function Prototypes                                                 */
/******************************************************************************/

/* Custom oscillator configuration funtions, reset source evaluation
functions, and other non-peripheral microcontroller initialization functions
go here. */

void ConfigureOscillator(void); /* Handles clock switching/osc initialization */
