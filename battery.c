/*
 * File:   battery.c
 * Author: dyulov
 *
 * Created on July 29, 2013, 10:27 AM
 *
 * Battery over-charge protection and any future battery related functionality.
 */
#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>

#include "types.h"
#include "fixed_point.h"
#include "linear_interp.h"
#include "config.h"
#include "peripherals.h"
#include "battery.h"

extern AnalogSignal_t AirTempADC_Filtered;
extern bool ChargeEnabled;

int16_t VsenseBattADC = 0;
int16_t BattTempCompOffset = 0;
struct LINEAR_INTERP_STRUCT BattTempCompInterp;
int16_t BattTempLo = BATT_TEMP_LO;
int16_t BattTempHi = BATT_TEMP_HI;
int16_t BattTempCompOffsetLo = BATT_TEMP_COMP_OFFSET_LO;
int16_t BattTempCompOffsetHi = BATT_TEMP_COMP_OFFSET_HI;
int16_t BattVolt25C_Lo = BATT_VOLT_25C_LO;
int16_t BattVolt25C_Hi = BATT_VOLT_25C_HI;

void BatteryMonitorInit(void){
    LinearInterpInit(
        &BattTempCompInterp,
        BattTempLo, BattTempCompOffsetLo,
        BattTempHi, BattTempCompOffsetHi,
        Q_IU);
}

// Called by scheduler
void BatteryMonitor(void){
    bool chargeSwitchState = CHRG_SWITCH_STATE; // FIXME: Not sure why I did this. Remove or explain.
    CHRG_DISCONNECT;
    BattTempCompOffset=0;
    // Get temperature compensation voltage offset from voltage at 25C
  /*  if(AirTempADC_Filtered.IsValid){
        BattTempCompOffset = LinearInterp(&BattTempCompInterp, AirTempADC_Filtered.Value);
    }else{
        BattTempCompOffset = 0;
    }*/
    VsenseBattADC = ADC_Convert(ADC_CH5); // Measure with battery disconnected
    if(VsenseBattADC > (BattVolt25C_Hi + BattTempCompOffset)){
        return;
    }
   if(ChargeEnabled){
        if((VsenseBattADC < (BattVolt25C_Lo + BattTempCompOffset)) || chargeSwitchState){  
            CHRG_CONNECT;
        }
   }
}